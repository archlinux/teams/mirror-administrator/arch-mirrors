import pathlib
from ..args import subparsers
from ..functions.build import build

parse_build = subparsers.add_parser("build", help="Builds a master TOML record of all mirrors")
parse_build.add_argument(
	"--output",
	required=False,
	default=pathlib.Path('./master.toml'),
	type=pathlib.Path,
	help="Where to store the master TOML file",
)
parse_build.add_argument(
	"--directory",
	required=False,
	default=pathlib.Path('./'),
	type=pathlib.Path,
	help="Where to look for TOML files as source information",
)

parse_build.set_defaults(func=build)